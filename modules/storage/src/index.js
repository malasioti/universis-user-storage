import {UserStorageService, UserStorage, UserStorageAccessConfiguration, DefaultUserStorageAccessConfiguration} from './service';

module.exports.UserStorageAccessConfiguration = UserStorageAccessConfiguration;
module.exports.DefaultUserStorageAccessConfiguration = DefaultUserStorageAccessConfiguration;
module.exports.UserStorageService = UserStorageService;
module.exports.UserStorage = UserStorage;
