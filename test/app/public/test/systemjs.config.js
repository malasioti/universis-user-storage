// noinspection ThisExpressionReferencesGlobalObjectJS
(function (global) {
    System.config({
        transpiler: 'ts',
        typescriptOptions: {
            tsconfig: true
        },
        meta: {
            'typescript': {
                "exports": "ts"
            }
        },
        paths: {
            // paths serve as alias
            'npm:': 'https://unpkg.com/'
        },
        // map tells the System loader where to look for things
        map: {
            // our app is within the app folder
            src: 'src',
            // typescript for compilation in the browser
            'ts':             'npm:plugin-typescript/lib/plugin.js',
            'typescript':     'npm:typescript/lib/typescript.js',
            'escape-string-regexp':'npm:escape-string-regexp@2.0.0/index.js',
            'mocha':     'npm:mocha@6.1.4/browser-entry.js'
        },
        // packages tells the System loader how to load when no filename and/or no extension
        packages: {
            "src": {
                "main": "./src/main.ts",
                "defaultExtension": "ts"
            }
        }
    });
// eslint-disable-next-line no-invalid-this
})(this);
