import request from 'supertest';
import {UserStorageService} from '../modules/storage/src/service';
import {assert} from 'chai';
import {ExpressDataApplication} from '@themost/express';
import app from './app/server/app';

describe('Test user storage endpoints', () => {

    before((done) => {
        /**
         * @type {ExpressDataApplication}
         */
        const dataApplication = app.get(ExpressDataApplication.name);
        // use service
        dataApplication.useStrategy(UserStorageService, UserStorageService);
        return done();
    });

    it('POST /api/users/me/storage/get', (done)=> {
        request(app)
            .post('/api/users/me/storage/get')
            .auth('admin', 'secret')
            .set('Accept', 'application/json')
            .send(
                {
                    key: "registrar"
                }
            )
            .end((err, response) => {
                assert.equal(response.status, 200);
                // eslint-disable-next-line no-console
                console.log('INFO', 'BODY', JSON.stringify(response.body, null, 4));
                assert.isObject(response.body);
                return done();
            });
    });

    it('POST /api/users/me/storage/set', (done)=> {
        request(app)
            .post('/api/users/me/storage/set')
            .auth('admin', 'secret')
            .send(
                {
                    key: "registrar",
                    value: {
                        "lastDepartment": "1200",
                        "lastAction": "/requests/active"
                    }
                }
            )
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json')
            .end((err, response) => {
            assert.equal(response.status, 200);
            assert.isObject(response.body);
            return done();
        });
    });

    it('POST /api/users/me/storage/get', (done)=> {
        request(app)
            .post('/api/users/me/storage/get')
            .send(
                {
                    key: "registrar/lastDepartment"
                }
            )
            .auth('admin', 'secret')
            .set('Accept', 'application/json')
            .end((err, response) => {
                assert.equal(response.status, 200);
                // eslint-disable-next-line no-console
                console.log('INFO', 'BODY', JSON.stringify(response.body, null, 4));
                assert.equal(response.body.value, '1200');
                return done();
            });
    });

    it('POST /api/users/me/storage/get', (done)=> {
        request(app)
            .post('/api/users/me/storage/get')
            .auth('admin', 'secret')
            .send(
                {
                    key: "registrar/lastRegistration"
                }
            )
            .set('Accept', 'application/json')
            .end((err, response) => {
                assert.equal(response.status, 200);
                // eslint-disable-next-line no-console
                console.log('INFO', 'BODY', JSON.stringify(response.body, null, 4));
                assert.equal(response.body.value, null);
                return done();
            });
    });


});
