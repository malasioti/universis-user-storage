import {UserStorageService, UserStorage} from '../modules/storage/src/service';
import {assert} from 'chai';
import {ExpressDataApplication} from '@themost/express';
import app from './app/server/app';
let User = require('./app/server/models/user-model');
describe('Test UserStorageService', () => {
    /**
     * @type {ExpressDataContext}
     */
    let context;
    before((done) => {
        /**
         * @type {ExpressDataApplication}
         */
        const dataApplication = app.get(ExpressDataApplication.name);
        // use service
        dataApplication.useStrategy(UserStorageService, UserStorageService);
        // create context
        context = dataApplication.createContext();
        context.user = {
            name: 'admin',
            authenticationScope: 'registrar'
        };
        return done();
    });

    it('should get storage item', async () => {
        const user = await User.getMe(context);
        const item = await user.storage.getItem('registrar');
        await user.storage.removeItem('registrar');
    });

    it('should set storage item', async () => {
        const user = await User.getMe(context);
        await user.storage.setItem('registrar', {});
        await user.storage.setItem('registrar/lastDepartment', '101');
        await user.storage.setItem('registrar/lastPath', '/requests/list');
        const lastDepartment = await user.storage.getItem('registrar/lastDepartment');
        assert.equal(lastDepartment, '101');
    });

    it('should throw access denied while getting storage item', async () => {
        const user = await User.getMe(context);
        return user.storage.getItem('application1')
            .then(
                () => Promise.reject(new Error('Expected method to reject.')),
                err => assert.instanceOf(err, Error)
            );
    });

});
